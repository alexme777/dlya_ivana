<?php
namespace app\models;

/**
 * This is the model class for table "slave".
 *
 * @property string $name
 * @property string $position
 * @property integer $active_from
 * @property integer $active
 */
class SlaveModel extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'slave';
    }
        
    public function rules()
    {
        return [
            [['name', 'position', 'active_from'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['position'], 'string', 'max' => 255],
            [['active_from'], 'filter', 'filter' => function ($value) {
                if (!preg_match("/^[\d\+]+$/", $value) && $value > 0) {
                    return strtotime($value);
                } else {
                    return $value;
                }
            }],
            [['active'], 'integer', 'max' => 11]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Фамилия, Имя, Отчество',
            'position' => 'Должность',
            'active_from' => 'Дата приема на работу',
            'active' => 'Активность',
        ];
    }
}
