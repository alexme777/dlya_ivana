<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SlaveSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <p>
            <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div> 
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                'position',
                [
                    'attribute' => 'active_from',
                    'format' => 'text',
                    'value' => function($dataProvider) {
                        return $dataProvider->active ? Yii::$app->formatter->asDate($dataProvider->active_from, 'php:d-m-Y') : '';
                    }
                ],
                [
                    'label' => 'Статус',
                    'format' => 'text',
                    'value' => function($dataProvider) {
                        return $dataProvider->active ? 'Работает' : 'Уволен';
                    }
                ],

                ['class' => 'yii\grid\ActionColumn', 'template'=>'{update} {delete}'],
            ],
        ]); ?>
    </div>
</div>
