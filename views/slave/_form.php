<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\SlaveModel */
/* @var $form yii\widgets\ActiveForm */

$active_from = !empty($model->active_from) ? Yii::$app->formatter->asDatetime($model->active_from, 'php:d-m-Y') : '';
?>

<div class="slave-model-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'active')->checkbox([
        'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
    ])?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'active_from')->widget(
        DatePicker::className(), [
            'options' => [
                'value' => $active_from,
            ],
            'pluginOptions' => [
                'autoclose' => TRUE,
                'format' => 'dd-mm-yyyy',
            ]
        ]
    )?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
