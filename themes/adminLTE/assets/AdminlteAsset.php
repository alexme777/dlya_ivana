<?php

namespace app\themes\adminLTE\assets;

use yii\web\AssetBundle;

class AdminlteAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte';
    public $css = [
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'dist/css/AdminLTE.min.css',
        'dist/css/skins/_all-skins.min.css',
		'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
		'plugins/iCheck/all.css'
    ];
    public $js = [
		'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'dist/js/adminlte.min.js',
		'bower_components/datatables.net/js/jquery.dataTables.min.js',
		'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
		'bower_components/ckeditor/ckeditor.js',
		'bower_components/ckeditor/config.js',
		'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
		'plugins/iCheck/icheck.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
