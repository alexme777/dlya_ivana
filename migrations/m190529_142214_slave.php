<?php

use yii\db\Migration;

/**
 * Class m190529_142214_slave
 */
class m190529_142214_slave extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('slave', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'position' => $this->string(),
			'active_from' => $this->integer(),
			'active' => $this->integer(),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('slave');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190529_142214_slave cannot be reverted.\n";

        return false;
    }
    */
}
